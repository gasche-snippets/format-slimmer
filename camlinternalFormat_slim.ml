(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                          Benoit Vaugon, ENSTA                          *)
(*                                                                        *)
(*   Copyright 2014 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open CamlinternalFormatBasics

(******************************************************************************)
           (* Tools to manipulate scanning set of chars (see %[...]) *)

type mutable_char_set = bytes

(* Create a fresh, empty, mutable char set. *)
let create_char_set () = Bytes.make 32 '\000'

(* Add a char in a mutable char set. *)
let add_in_char_set char_set c =
  let ind = int_of_char c in
  let str_ind = ind lsr 3 and mask = 1 lsl (ind land 0b111) in
  Bytes.set char_set str_ind
    (char_of_int (int_of_char (Bytes.get char_set str_ind) lor mask))

let freeze_char_set char_set =
  Bytes.to_string char_set

(* Compute the complement of a char set. *)
let rev_char_set char_set =
  let char_set' = create_char_set () in
  for i = 0 to 31 do
    Bytes.set char_set' i
      (char_of_int (int_of_char (String.get char_set i) lxor 0xFF));
  done;
  Bytes.unsafe_to_string char_set'

(* Return true if a `c' is in `char_set'. *)
let is_in_char_set char_set c =
  let ind = int_of_char c in
  let str_ind = ind lsr 3 and mask = 1 lsl (ind land 0b111) in
  (int_of_char (String.get char_set str_ind) land mask) <> 0


(******************************************************************************)
                                 (* Types *)

type ('b, 'c) acc_formatting_gen =
  | Acc_open_tag of ('b, 'c) acc
  | Acc_open_box of ('b, 'c) acc

(* Reversed list of printing atoms. *)
(* Used to accumulate printf arguments. *)
and ('b, 'c) acc =
  | Acc_formatting_lit of ('b, 'c) acc * formatting_lit
      (* Special fmtting (box) *)
  | Acc_formatting_gen of ('b, 'c) acc * ('b, 'c) acc_formatting_gen
      (* Special fmtting (box) *)
  | Acc_string_literal of ('b, 'c) acc * string     (* Literal string *)
  | Acc_char_literal   of ('b, 'c) acc * char       (* Literal char *)
  | Acc_data_string    of ('b, 'c) acc * string     (* Generated string *)
  | Acc_data_char      of ('b, 'c) acc * char       (* Generated char *)
  | Acc_delay          of ('b, 'c) acc * ('b -> 'c)
                                                (* Delayed printing (%a, %t) *)
  | Acc_flush          of ('b, 'c) acc              (* Flush *)
  | Acc_invalid_arg    of ('b, 'c) acc * string
      (* Raise Invalid_argument msg *)
  | End_of_acc

(* List of heterogeneous values. *)
(* Used to accumulate scanf callback arguments. *)
type ('a, 'b) heter_list =
  | Cons : 'c * ('a, 'b) heter_list -> ('c -> 'a, 'b) heter_list
  | Nil : ('b, 'b) heter_list

(******************************************************************************)
                               (* Constants *)

(* Default precision for float printing. *)
let default_float_precision fconv =
  match snd fconv with
  | Float_f | Float_e | Float_E | Float_g | Float_G | Float_h | Float_H
  | Float_CF -> -6
  (* For %h %H and %#F formats, a negative precision means "as many digits as
     necessary".  For the other FP formats, we take the absolute value
     of the precision, hence 6 digits by default. *)
  | Float_F -> 12
  (* Default precision for OCaml float printing (%F). *)

(******************************************************************************)
                               (* Externals *)

external format_float: string -> float -> string
  = "caml_format_float"
external format_int: string -> int -> string
  = "caml_format_int"
external format_int32: string -> int32 -> string
  = "caml_int32_format"
external format_nativeint: string -> nativeint -> string
  = "caml_nativeint_format"
external format_int64: string -> int64 -> string
  = "caml_int64_format"
external hexstring_of_float: float -> int -> char -> string
  = "caml_hexstring_of_float"

(******************************************************************************)
                     (* Tools to pretty-print formats *)

(* Type of extensible character buffers. *)
type buffer = {
  mutable ind : int;
  mutable bytes : bytes;
}

(* Create a fresh buffer. *)
let buffer_create init_size = { ind = 0; bytes = Bytes.create init_size }

(* Check size of the buffer and grow it if needed. *)
let buffer_check_size buf overhead =
  let len = Bytes.length buf.bytes in
  let min_len = buf.ind + overhead in
  if min_len > len then (
    let new_len = Int.max (len * 2) min_len in
    let new_str = Bytes.create new_len in
    Bytes.blit buf.bytes 0 new_str 0 len;
    buf.bytes <- new_str;
  )

(* Add the character `c' to the buffer `buf'. *)
let buffer_add_char buf c =
  buffer_check_size buf 1;
  Bytes.set buf.bytes buf.ind c;
  buf.ind <- buf.ind + 1

(* Add the string `s' to the buffer `buf'. *)
let buffer_add_string buf s =
  let str_len = String.length s in
  buffer_check_size buf str_len;
  String.blit s 0 buf.bytes buf.ind str_len;
  buf.ind <- buf.ind + str_len

(* Get the content of the buffer. *)
let buffer_contents buf =
  Bytes.sub_string buf.bytes 0 buf.ind

(***)

(* Convert an integer conversion to char. *)
let char_of_iconv iconv = match iconv with
  | Int_d | Int_pd | Int_sd | Int_Cd -> 'd' | Int_i | Int_pi | Int_si
  | Int_Ci -> 'i' | Int_x | Int_Cx -> 'x' | Int_X | Int_CX -> 'X' | Int_o
  | Int_Co -> 'o' | Int_u | Int_Cu -> 'u'

(* Convert a float conversion to char. *)
(* `cF' will be 'F' for displaying format and 'g' to call libc printf *)
let char_of_fconv ?(cF='F') fconv = match snd fconv with
  | Float_f -> 'f' | Float_e -> 'e'
  | Float_E -> 'E' | Float_g -> 'g'
  | Float_G -> 'G' | Float_F -> cF
  | Float_h -> 'h' | Float_H -> 'H'
  | Float_CF -> 'F'

(* Print the optional '+', ' ' and/or '#' associated to a float conversion. *)
let bprint_fconv_flag buf fconv =
  begin match fst fconv with
  | Float_flag_p -> buffer_add_char buf '+'
  | Float_flag_s -> buffer_add_char buf ' '
  | Float_flag_ -> () end;
  match snd fconv with
  | Float_CF -> buffer_add_char buf '#'
  | Float_f | Float_e | Float_E | Float_g | Float_G
  | Float_F | Float_h | Float_H -> ()

(* Compute the literal string representation of a Formatting_lit. *)
(* Used by Printf and Scanf where formatting is not interpreted. *)
let string_of_formatting_lit formatting_lit = match formatting_lit with
  | Close_box            -> "@]"
  | Close_tag            -> "@}"
  | Break (str, _, _)    -> str
  | FFlush               -> "@?"
  | Force_newline        -> "@\n"
  | Flush_newline        -> "@."
  | Magic_size (str, _)  -> str
  | Escaped_at           -> "@@"
  | Escaped_percent      -> "@%"
  | Scan_indic c -> "@" ^ (String.make 1 c)

(******************************************************************************)
                             (* Printing tools *)

(* Add padding spaces around a string. *)
let fix_padding padty width str =
  let len = String.length str in
  let width, padty =
    abs width,
    (* while literal padding widths are always non-negative,
       dynamically-set widths (Arg_padding, eg. %*d) may be negative;
       we interpret those as specifying a padding-to-the-left; this
       means that '0' may get dropped even if it was explicitly set,
       but:
       - this is what the legacy implementation does, and
         we preserve compatibility if possible
       - we could only signal this issue by failing at runtime,
         which is not very nice... *)
    if width < 0 then Left else padty in
  if width <= len then str else
    let res = Bytes.make width (if padty = Zeros then '0' else ' ') in
    begin match padty with
    | Left  -> String.blit str 0 res 0 len
    | Right -> String.blit str 0 res (width - len) len
    | Zeros when len > 0 && (str.[0] = '+' || str.[0] = '-' || str.[0] = ' ') ->
      Bytes.set res 0 str.[0];
      String.blit str 1 res (width - len + 1) (len - 1)
    | Zeros when len > 1 && str.[0] = '0' && (str.[1] = 'x' || str.[1] = 'X') ->
      Bytes.set res 1 str.[1];
      String.blit str 2 res (width - len + 2) (len - 2)
    | Zeros ->
      String.blit str 0 res (width - len) len
    end;
    Bytes.unsafe_to_string res

(* Add '0' padding to int, int32, nativeint or int64 string representation. *)
let fix_int_precision prec str =
  let prec = abs prec in
  let len = String.length str in
  match str.[0] with
  | ('+' | '-' | ' ') as c when prec + 1 > len ->
    let res = Bytes.make (prec + 1) '0' in
    Bytes.set res 0 c;
    String.blit str 1 res (prec - len + 2) (len - 1);
    Bytes.unsafe_to_string res
  | '0' when prec + 2 > len && len > 1 && (str.[1] = 'x' || str.[1] = 'X') ->
    let res = Bytes.make (prec + 2) '0' in
    Bytes.set res 1 str.[1];
    String.blit str 2 res (prec - len + 4) (len - 2);
    Bytes.unsafe_to_string res
  | '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' when prec > len ->
    let res = Bytes.make prec '0' in
    String.blit str 0 res (prec - len) len;
    Bytes.unsafe_to_string res
  | _ ->
    str

(* Escape a string according to the OCaml lexing convention. *)
let string_to_caml_string str =
  let str = String.escaped str in
  let l = String.length str in
  let res = Bytes.make (l + 2) '\"' in
  String.unsafe_blit str 0 res 1 l;
  Bytes.unsafe_to_string res

(* Generate the format_int/int32/nativeint/int64 first argument
   from an int_conv. *)
let format_of_iconv = function
  | Int_d | Int_Cd -> "%d" | Int_pd -> "%+d" | Int_sd -> "% d"
  | Int_i | Int_Ci -> "%i" | Int_pi -> "%+i" | Int_si -> "% i"
  | Int_x -> "%x" | Int_Cx -> "%#x"
  | Int_X -> "%X" | Int_CX -> "%#X"
  | Int_o -> "%o" | Int_Co -> "%#o"
  | Int_u | Int_Cu -> "%u"

let format_of_iconvL = function
  | Int_d | Int_Cd -> "%Ld" | Int_pd -> "%+Ld" | Int_sd -> "% Ld"
  | Int_i | Int_Ci -> "%Li" | Int_pi -> "%+Li" | Int_si -> "% Li"
  | Int_x -> "%Lx" | Int_Cx -> "%#Lx"
  | Int_X -> "%LX" | Int_CX -> "%#LX"
  | Int_o -> "%Lo" | Int_Co -> "%#Lo"
  | Int_u | Int_Cu -> "%Lu"

let format_of_iconvl = function
  | Int_d | Int_Cd -> "%ld" | Int_pd -> "%+ld" | Int_sd -> "% ld"
  | Int_i | Int_Ci -> "%li" | Int_pi -> "%+li" | Int_si -> "% li"
  | Int_x -> "%lx" | Int_Cx -> "%#lx"
  | Int_X -> "%lX" | Int_CX -> "%#lX"
  | Int_o -> "%lo" | Int_Co -> "%#lo"
  | Int_u | Int_Cu -> "%lu"

let format_of_iconvn = function
  | Int_d | Int_Cd -> "%nd" | Int_pd -> "%+nd" | Int_sd -> "% nd"
  | Int_i | Int_Ci -> "%ni" | Int_pi -> "%+ni" | Int_si -> "% ni"
  | Int_x -> "%nx" | Int_Cx -> "%#nx"
  | Int_X -> "%nX" | Int_CX -> "%#nX"
  | Int_o -> "%no" | Int_Co -> "%#no"
  | Int_u | Int_Cu -> "%nu"

(* Generate the format_float first argument from a float_conv. *)
let format_of_fconv fconv prec =
    let prec = abs prec in
    let symb = char_of_fconv ~cF:'g' fconv in
    let buf = buffer_create 16 in
    buffer_add_char buf '%';
    bprint_fconv_flag buf fconv;
    buffer_add_char buf '.';
    buffer_add_string buf (Int.to_string prec);
    buffer_add_char buf symb;
    buffer_contents buf

let transform_int_alt iconv s =
  match iconv with
  | Int_Cd | Int_Ci | Int_Cu ->
    let digits =
      let n = ref 0 in
      for i = 0 to String.length s - 1 do
        match String.unsafe_get s i with
        | '0'..'9' -> incr n
        | _ -> ()
      done;
      !n
    in
    let buf = Bytes.create (String.length s + (digits - 1) / 3) in
    let pos = ref 0 in
    let put c = Bytes.set buf !pos c; incr pos in
    let left = ref ((digits - 1) mod 3 + 1) in
    for i = 0 to String.length s - 1 do
      match String.unsafe_get s i with
      | '0'..'9' as c ->
          if !left = 0 then (put '_'; left := 3); decr left; put c
      | c -> put c
    done;
    Bytes.unsafe_to_string buf
  | _ -> s

(* Convert an integer to a string according to a conversion. *)
let convert_int iconv n =
  transform_int_alt iconv (format_int (format_of_iconv iconv) n)
let convert_int32 iconv n =
  transform_int_alt iconv (format_int32 (format_of_iconvl iconv) n)
let convert_nativeint iconv n =
  transform_int_alt iconv (format_nativeint (format_of_iconvn iconv) n)
let convert_int64 iconv n =
  transform_int_alt iconv (format_int64 (format_of_iconvL iconv) n)

(* Convert a float to string. *)
(* Fix special case of "OCaml float format". *)
let convert_float fconv prec x =
  let hex () =
    let sign =
      match fst fconv with
      | Float_flag_p -> '+'
      | Float_flag_s -> ' '
      | _ -> '-' in
    hexstring_of_float x prec sign in
  let add_dot_if_needed str =
    let len = String.length str in
    let rec is_valid i =
      if i = len then false else
        match str.[i] with
        | '.' | 'e' | 'E' -> true
        | _ -> is_valid (i + 1) in
    if is_valid 0 then str else str ^ "." in
  let caml_special_val str = match classify_float x with
    | FP_normal | FP_subnormal | FP_zero -> str
    | FP_infinite -> if x < 0.0 then "neg_infinity" else "infinity"
    | FP_nan -> "nan" in
  match snd fconv with
  | Float_h -> hex ()
  | Float_H -> String.uppercase_ascii (hex ())
  | Float_CF -> caml_special_val (hex ())
  | Float_F ->
    let str = format_float (format_of_fconv fconv prec) x in
    caml_special_val (add_dot_if_needed str)
  | Float_f | Float_e | Float_E | Float_g | Float_G ->
    format_float (format_of_fconv fconv prec) x

(* Convert a char to a string according to the OCaml lexical convention. *)
let format_caml_char c =
  let str = Char.escaped c in
  let l = String.length str in
  let res = Bytes.make (l + 2) '\'' in
  String.unsafe_blit str 0 res 1 l;
  Bytes.unsafe_to_string res

(******************************************************************************)
                        (* Generic printing function *)

(* Make a generic printing function. *)
(* Used to generate Printf and Format printing functions. *)
(* Parameters:
     k: a continuation finally applied to the output stream and the accumulator.
     o: the output stream (see k, %a and %t).
     acc: rev list of printing entities (string, char, flush, formatting, ...).
     fmt: the format. *)
let rec make_printf : type a b c d e f .
    ((b, c) acc -> f) -> (b, c) acc ->
    (a, b, c, d, e, f) fmt -> a =
fun k acc fmt -> match fmt with
  | Char rest ->
    fun c ->
      let new_acc = Acc_data_char (acc, c) in
      make_printf k new_acc rest
  | Caml_char rest ->
    fun c ->
      let new_acc = Acc_data_string (acc, format_caml_char c) in
      make_printf k new_acc rest
  | String (pad, rest) ->
    make_padding k acc rest pad (fun str -> str)
  | Caml_string (pad, rest) ->
    make_padding k acc rest pad string_to_caml_string
  | Int (iconv, pad, prec, rest) ->
    make_int_padding_precision k acc rest pad prec convert_int iconv
  | Int32 (iconv, pad, prec, rest) ->
    make_int_padding_precision k acc rest pad prec convert_int32 iconv
  | Nativeint (iconv, pad, prec, rest) ->
    make_int_padding_precision k acc rest pad prec convert_nativeint iconv
  | Int64 (iconv, pad, prec, rest) ->
    make_int_padding_precision k acc rest pad prec convert_int64 iconv
  | Float (fconv, pad, prec, rest) ->
    make_float_padding_precision k acc rest pad prec fconv
  | Bool (pad, rest) ->
    make_padding k acc rest pad string_of_bool
  | Alpha rest ->
    fun f x -> make_printf k (Acc_delay (acc, fun o -> f o x)) rest
  | Theta rest ->
    fun f -> make_printf k (Acc_delay (acc, f)) rest
  | Custom (arity, f, rest) ->
    make_custom k acc rest arity (f ())
  | Reader _ ->
    (* This case is impossible, by typing of formats. *)
    (* Indeed, since printf and co. take a format4 as argument, the 'd and 'e
       type parameters of fmt are obviously equals. The Reader is the
       only constructor which touch 'd and 'e type parameters of the format
       type, it adds an (->) to the 'd parameters. Consequently, a format4
       cannot contain a Reader node, except in the sub-format associated to
       an %{...%}. It's not a problem because make_printf do not call
       itself recursively on the sub-format associated to %{...%}. *)
    assert false
  | Flush rest ->
    make_printf k (Acc_flush acc) rest

  | String_literal (str, rest) ->
    make_printf k (Acc_string_literal (acc, str)) rest
  | Char_literal (chr, rest) ->
    make_printf k (Acc_char_literal (acc, chr)) rest

  | Format_arg (_, sub_fmtty, rest) -> failwith "unsupported"
  | Format_subst (_, fmtty, rest) -> failwith "unsupported"
  | Scan_char_set (_, _, rest) ->
    let new_acc = Acc_invalid_arg (acc, "Printf: bad conversion %[") in
    fun _ -> make_printf k new_acc rest
  | Scan_get_counter (_, rest) ->
    (* This case should be refused for Printf. *)
    (* Accepted for backward compatibility. *)
    (* Interpret %l, %n and %L as %u. *)
    fun n ->
      let new_acc = Acc_data_string (acc, format_int "%u" n) in
      make_printf k new_acc rest
  | Scan_next_char rest ->
    fun c ->
      let new_acc = Acc_data_char (acc, c) in
      make_printf k new_acc rest
  | Ignored_param (ign, rest) ->
    make_ignored_param k acc ign rest

  | Formatting_lit (fmting_lit, rest) ->
    make_printf k (Acc_formatting_lit (acc, fmting_lit)) rest
  | Formatting_gen (Open_tag (Format (fmt', _)), rest) ->
    let k' kacc =
      make_printf k (Acc_formatting_gen (acc, Acc_open_tag kacc)) rest in
    make_printf k' End_of_acc fmt'
  | Formatting_gen (Open_box (Format (fmt', _)), rest) ->
    let k' kacc =
      make_printf k (Acc_formatting_gen (acc, Acc_open_box kacc)) rest in
    make_printf k' End_of_acc fmt'

  | End_of_format ->
    k acc

(* Delay the error (Invalid_argument "Printf: bad conversion %_"). *)
(* Generate functions to take remaining arguments (after the "%_"). *)
and make_ignored_param : type x y a b c d e f .
    ((b, c) acc -> f) -> (b, c) acc ->
    (a, b, c, d, y, x) ignored ->
    (x, b, c, y, e, f) fmt -> a =
fun k acc ign fmt -> match ign with
  | Ignored_char                    -> make_invalid_arg k acc fmt
  | Ignored_caml_char               -> make_invalid_arg k acc fmt
  | Ignored_string _                -> make_invalid_arg k acc fmt
  | Ignored_caml_string _           -> make_invalid_arg k acc fmt
  | Ignored_int (_, _)              -> make_invalid_arg k acc fmt
  | Ignored_int32 (_, _)            -> make_invalid_arg k acc fmt
  | Ignored_nativeint (_, _)        -> make_invalid_arg k acc fmt
  | Ignored_int64 (_, _)            -> make_invalid_arg k acc fmt
  | Ignored_float (_, _)            -> make_invalid_arg k acc fmt
  | Ignored_bool _                  -> make_invalid_arg k acc fmt
  | Ignored_format_arg _            -> make_invalid_arg k acc fmt
  | Ignored_format_subst (_, fmtty) -> make_from_fmtty k acc fmtty fmt
  | Ignored_reader                  -> assert false
  | Ignored_scan_char_set _         -> make_invalid_arg k acc fmt
  | Ignored_scan_get_counter _      -> make_invalid_arg k acc fmt
  | Ignored_scan_next_char          -> make_invalid_arg k acc fmt


(* Special case of printf "%_(". *)
and make_from_fmtty : type x y a b c d e f .
    ((b, c) acc -> f) -> (b, c) acc ->
    (a, b, c, d, y, x) fmtty ->
    (x, b, c, y, e, f) fmt -> a =
fun k acc fmtty fmt -> match fmtty with
  | Char_ty rest            -> fun _ -> make_from_fmtty k acc rest fmt
  | String_ty rest          -> fun _ -> make_from_fmtty k acc rest fmt
  | Int_ty rest             -> fun _ -> make_from_fmtty k acc rest fmt
  | Int32_ty rest           -> fun _ -> make_from_fmtty k acc rest fmt
  | Nativeint_ty rest       -> fun _ -> make_from_fmtty k acc rest fmt
  | Int64_ty rest           -> fun _ -> make_from_fmtty k acc rest fmt
  | Float_ty rest           -> fun _ -> make_from_fmtty k acc rest fmt
  | Bool_ty rest            -> fun _ -> make_from_fmtty k acc rest fmt
  | Alpha_ty rest           -> fun _ _ -> make_from_fmtty k acc rest fmt
  | Theta_ty rest           -> fun _ -> make_from_fmtty k acc rest fmt
  | Any_ty rest             -> fun _ -> make_from_fmtty k acc rest fmt
  | Reader_ty _             -> assert false
  | Ignored_reader_ty _     -> assert false
  | Format_arg_ty (_, rest) -> fun _ -> make_from_fmtty k acc rest fmt
  | End_of_fmtty            -> make_invalid_arg k acc fmt
  | Format_subst_ty (ty1, ty2, rest) -> failwith "unsupported"

(* Insert an Acc_invalid_arg in the accumulator and continue to generate
   closures to get the remaining arguments. *)
and make_invalid_arg : type a b c d e f .
    ((b, c) acc -> f) -> (b, c) acc ->
    (a, b, c, d, e, f) fmt -> a =
fun k acc fmt ->
  make_printf k (Acc_invalid_arg (acc, "Printf: bad conversion %_")) fmt

(* Fix padding, take it as an extra integer argument if needed. *)
and make_padding : type x z a b c d e f .
    ((b, c) acc -> f) -> (b, c) acc ->
    (a, b, c, d, e, f) fmt ->
    (x, z -> a) padding -> (z -> string) -> x =
  fun k acc fmt pad trans -> match pad with
  | No_padding ->
    fun x ->
      let new_acc = Acc_data_string (acc, trans x) in
      make_printf k new_acc fmt
  | Lit_padding (padty, width) ->
    fun x ->
      let new_acc = Acc_data_string (acc, fix_padding padty width (trans x)) in
      make_printf k new_acc fmt
  | Arg_padding padty ->
    fun w x ->
      let new_acc = Acc_data_string (acc, fix_padding padty w (trans x)) in
      make_printf k new_acc fmt

(* Fix padding and precision for int, int32, nativeint or int64. *)
(* Take one or two extra integer arguments if needed. *)
and make_int_padding_precision : type x y z a b c d e f .
    ((b, c) acc -> f) -> (b, c) acc ->
    (a, b, c, d, e, f) fmt ->
    (x, y) padding -> (y, z -> a) precision -> (int_conv -> z -> string) ->
    int_conv -> x =
  fun k acc fmt pad prec trans iconv -> match pad, prec with
  | No_padding, No_precision ->
    fun x ->
      let str = trans iconv x in
      make_printf k (Acc_data_string (acc, str)) fmt
  | No_padding, Lit_precision p ->
    fun x ->
      let str = fix_int_precision p (trans iconv x) in
      make_printf k (Acc_data_string (acc, str)) fmt
  | No_padding, Arg_precision ->
    fun p x ->
      let str = fix_int_precision p (trans iconv x) in
      make_printf k (Acc_data_string (acc, str)) fmt
  | Lit_padding (padty, w), No_precision ->
    fun x ->
      let str = fix_padding padty w (trans iconv x) in
      make_printf k (Acc_data_string (acc, str)) fmt
  | Lit_padding (padty, w), Lit_precision p ->
    fun x ->
      let str = fix_padding padty w (fix_int_precision p (trans iconv x)) in
      make_printf k (Acc_data_string (acc, str)) fmt
  | Lit_padding (padty, w), Arg_precision ->
    fun p x ->
      let str = fix_padding padty w (fix_int_precision p (trans iconv x)) in
      make_printf k (Acc_data_string (acc, str)) fmt
  | Arg_padding padty, No_precision ->
    fun w x ->
      let str = fix_padding padty w (trans iconv x) in
      make_printf k (Acc_data_string (acc, str)) fmt
  | Arg_padding padty, Lit_precision p ->
    fun w x ->
      let str = fix_padding padty w (fix_int_precision p (trans iconv x)) in
      make_printf k (Acc_data_string (acc, str)) fmt
  | Arg_padding padty, Arg_precision ->
    fun w p x ->
      let str = fix_padding padty w (fix_int_precision p (trans iconv x)) in
      make_printf k (Acc_data_string (acc, str)) fmt

(* Convert a float, fix padding and precision if needed. *)
(* Take the float argument and one or two extra integer arguments if needed. *)
and make_float_padding_precision : type x y a b c d e f .
    ((b, c) acc -> f) -> (b, c) acc ->
    (a, b, c, d, e, f) fmt ->
    (x, y) padding -> (y, float -> a) precision -> float_conv -> x =
  fun k acc fmt pad prec fconv -> match pad, prec with
  | No_padding, No_precision ->
    fun x ->
      let str = convert_float fconv (default_float_precision fconv) x in
      make_printf k (Acc_data_string (acc, str)) fmt
  | No_padding, Lit_precision p ->
    fun x ->
      let str = convert_float fconv p x in
      make_printf k (Acc_data_string (acc, str)) fmt
  | No_padding, Arg_precision ->
    fun p x ->
      let str = convert_float fconv p x in
      make_printf k (Acc_data_string (acc, str)) fmt
  | Lit_padding (padty, w), No_precision ->
    fun x ->
      let str = convert_float fconv (default_float_precision fconv) x in
      let str' = fix_padding padty w str in
      make_printf k (Acc_data_string (acc, str')) fmt
  | Lit_padding (padty, w), Lit_precision p ->
    fun x ->
      let str = fix_padding padty w (convert_float fconv p x) in
      make_printf k (Acc_data_string (acc, str)) fmt
  | Lit_padding (padty, w), Arg_precision ->
    fun p x ->
      let str = fix_padding padty w (convert_float fconv p x) in
      make_printf k (Acc_data_string (acc, str)) fmt
  | Arg_padding padty, No_precision ->
    fun w x ->
      let str = convert_float fconv (default_float_precision fconv) x in
      let str' = fix_padding padty w str in
      make_printf k (Acc_data_string (acc, str')) fmt
  | Arg_padding padty, Lit_precision p ->
    fun w x ->
      let str = fix_padding padty w (convert_float fconv p x) in
      make_printf k (Acc_data_string (acc, str)) fmt
  | Arg_padding padty, Arg_precision ->
    fun w p x ->
      let str = fix_padding padty w (convert_float fconv p x) in
      make_printf k (Acc_data_string (acc, str)) fmt
and make_custom : type x y a b c d e f .
  ((b, c) acc -> f) -> (b, c) acc ->
  (a, b, c, d, e, f) fmt ->
  (a, x, y) custom_arity -> x -> y =
  fun k acc rest arity f -> match arity with
  | Custom_zero -> make_printf k (Acc_data_string (acc, f)) rest
  | Custom_succ arity ->
    fun x ->
      make_custom k acc rest arity (f x)

let const x _ = x

let rec make_iprintf : type a b c d e f state.
  (state -> f) -> state -> (a, b, c, d, e, f) fmt -> a =
  fun k o fmt -> match fmt with
    | Char rest ->
        const (make_iprintf k o rest)
    | Caml_char rest ->
        const (make_iprintf k o rest)
    | String (No_padding, rest) ->
        const (make_iprintf k o rest)
    | String (Lit_padding _, rest) ->
        const (make_iprintf k o rest)
    | String (Arg_padding _, rest) ->
        const (const (make_iprintf k o rest))
    | Caml_string (No_padding, rest) ->
        const (make_iprintf k o rest)
    | Caml_string (Lit_padding _, rest) ->
        const (make_iprintf k o rest)
    | Caml_string (Arg_padding _, rest) ->
        const (const (make_iprintf k o rest))
    | Int (_, pad, prec, rest) ->
        fn_of_padding_precision k o rest pad prec
    | Int32 (_, pad, prec, rest) ->
        fn_of_padding_precision k o rest pad prec
    | Nativeint (_, pad, prec, rest) ->
        fn_of_padding_precision k o rest pad prec
    | Int64 (_, pad, prec, rest) ->
        fn_of_padding_precision k o rest pad prec
    | Float (_, pad, prec, rest) ->
        fn_of_padding_precision k o rest pad prec
    | Bool (No_padding, rest) ->
        const (make_iprintf k o rest)
    | Bool (Lit_padding _, rest) ->
        const (make_iprintf k o rest)
    | Bool (Arg_padding _, rest) ->
        const (const (make_iprintf k o rest))
    | Alpha rest ->
        const (const (make_iprintf k o rest))
    | Theta rest ->
        const (make_iprintf k o rest)
    | Custom (arity, _, rest) ->
        fn_of_custom_arity k o rest arity
    | Reader _ ->
        (* This case is impossible, by typing of formats.  See the
           note in the corresponding case for make_printf. *)
        assert false
    | Flush rest ->
        make_iprintf k o rest
    | String_literal (_, rest) ->
        make_iprintf k o rest
    | Char_literal (_, rest) ->
        make_iprintf k o rest
    | Format_arg (_, _, rest) -> failwith "unsupported"
    | Format_subst (_, fmtty, rest) -> failwith "unsupported"
    | Scan_char_set (_, _, rest) ->
        const (make_iprintf k o rest)
    | Scan_get_counter (_, rest) ->
        const (make_iprintf k o rest)
    | Scan_next_char rest ->
        const (make_iprintf k o rest)
    | Ignored_param (ign, rest) ->
        make_ignored_param (fun _ -> k o) (End_of_acc) ign rest
    | Formatting_lit (_, rest) ->
        make_iprintf k o rest
    | Formatting_gen (Open_tag (Format (fmt', _)), rest) ->
        make_iprintf (fun koc -> make_iprintf k koc rest) o fmt'
    | Formatting_gen (Open_box (Format (fmt', _)), rest) ->
        make_iprintf (fun koc -> make_iprintf k koc rest) o fmt'
    | End_of_format ->
        k o
and fn_of_padding_precision :
  type x y z a b c d e f state.
  (state -> f) -> state -> (a, b, c, d, e, f) fmt ->
  (x, y) padding -> (y, z -> a) precision -> x =
  fun k o fmt pad prec -> match pad, prec with
    | No_padding   , No_precision    ->
        const (make_iprintf k o fmt)
    | No_padding   , Lit_precision _ ->
        const (make_iprintf k o fmt)
    | No_padding   , Arg_precision   ->
        const (const (make_iprintf k o fmt))
    | Lit_padding _, No_precision    ->
        const (make_iprintf k o fmt)
    | Lit_padding _, Lit_precision _ ->
        const (make_iprintf k o fmt)
    | Lit_padding _, Arg_precision   ->
        const (const (make_iprintf k o fmt))
    | Arg_padding _, No_precision    ->
        const (const (make_iprintf k o fmt))
    | Arg_padding _, Lit_precision _ ->
        const (const (make_iprintf k o fmt))
    | Arg_padding _, Arg_precision   ->
        const (const (const (make_iprintf k o fmt)))
and fn_of_custom_arity : type x y a b c d e f state.
  (state -> f) ->
  state -> (a, b, c, d, e, f) fmt -> (a, x, y) custom_arity -> y =
  fun k o fmt -> function
    | Custom_zero ->
        make_iprintf k o fmt
    | Custom_succ arity ->
        const (fn_of_custom_arity k o fmt arity)

(******************************************************************************)
                          (* Continuations for make_printf *)

(* Recursively output an "accumulator" containing a reversed list of
   printing entities (string, char, flus, ...) in an output_stream. *)
(* Used as a continuation of make_printf. *)
let rec output_acc o acc = match acc with
  | Acc_formatting_lit (p, fmting_lit) ->
    let s = string_of_formatting_lit fmting_lit in
    output_acc o p; output_string o s;
  | Acc_formatting_gen (p, Acc_open_tag acc') ->
    output_acc o p; output_string o "@{"; output_acc o acc';
  | Acc_formatting_gen (p, Acc_open_box acc') ->
    output_acc o p; output_string o "@["; output_acc o acc';
  | Acc_string_literal (p, s)
  | Acc_data_string (p, s)   -> output_acc o p; output_string o s
  | Acc_char_literal (p, c)
  | Acc_data_char (p, c)     -> output_acc o p; output_char o c
  | Acc_delay (p, f)         -> output_acc o p; f o
  | Acc_flush p              -> output_acc o p; flush o
  | Acc_invalid_arg (p, msg) -> output_acc o p; invalid_arg msg;
  | End_of_acc               -> ()

(* Recursively output an "accumulator" containing a reversed list of
   printing entities (string, char, flus, ...) in a buffer. *)
(* Used as a continuation of make_printf. *)
let rec bufput_acc b acc = match acc with
  | Acc_formatting_lit (p, fmting_lit) ->
    let s = string_of_formatting_lit fmting_lit in
    bufput_acc b p; Buffer.add_string b s;
  | Acc_formatting_gen (p, Acc_open_tag acc') ->
    bufput_acc b p; Buffer.add_string b "@{"; bufput_acc b acc';
  | Acc_formatting_gen (p, Acc_open_box acc') ->
    bufput_acc b p; Buffer.add_string b "@["; bufput_acc b acc';
  | Acc_string_literal (p, s)
  | Acc_data_string (p, s)   -> bufput_acc b p; Buffer.add_string b s
  | Acc_char_literal (p, c)
  | Acc_data_char (p, c)     -> bufput_acc b p; Buffer.add_char b c
  | Acc_delay (p, f)         -> bufput_acc b p; f b
  | Acc_flush p              -> bufput_acc b p;
  | Acc_invalid_arg (p, msg) -> bufput_acc b p; invalid_arg msg;
  | End_of_acc               -> ()

(* Recursively output an "accumulator" containing a reversed list of
   printing entities (string, char, flus, ...) in a buffer. *)
(* Differ from bufput_acc by the interpretation of %a and %t. *)
(* Used as a continuation of make_printf. *)
let rec strput_acc b acc = match acc with
  | Acc_formatting_lit (p, fmting_lit) ->
    let s = string_of_formatting_lit fmting_lit in
    strput_acc b p; Buffer.add_string b s;
  | Acc_formatting_gen (p, Acc_open_tag acc') ->
    strput_acc b p; Buffer.add_string b "@{"; strput_acc b acc';
  | Acc_formatting_gen (p, Acc_open_box acc') ->
    strput_acc b p; Buffer.add_string b "@["; strput_acc b acc';
  | Acc_string_literal (p, s)
  | Acc_data_string (p, s)   -> strput_acc b p; Buffer.add_string b s
  | Acc_char_literal (p, c)
  | Acc_data_char (p, c)     -> strput_acc b p; Buffer.add_char b c
  | Acc_delay (p, f)         -> strput_acc b p; Buffer.add_string b (f ())
  | Acc_flush p              -> strput_acc b p;
  | Acc_invalid_arg (p, msg) -> strput_acc b p; invalid_arg msg;
  | End_of_acc               -> ()

(******************************************************************************)
                          (* Error management *)

(* Raise [Failure] with a pretty-printed error message. *)
let failwith_message (Format (fmt, _)) =
  let buf = Buffer.create 256 in
  let k acc = strput_acc buf acc; failwith (Buffer.contents buf) in
  make_printf k End_of_acc fmt

(******************************************************************************)
                            (* Formatting tools *)

(* Convert a string to an open block description (indent, block_type) *)
let open_box_of_string str =
  if str = "" then (0, Pp_box) else
    let len = String.length str in
    let invalid_box () = failwith_message "invalid box description %S" str in
    let rec parse_spaces i =
      if i = len then i else
        match str.[i] with
        | ' ' | '\t' -> parse_spaces (i + 1)
        | _ -> i
    and parse_lword i j =
      if j = len then j else
        match str.[j] with
        | 'a' .. 'z' -> parse_lword i (j + 1)
        | _ -> j
    and parse_int i j =
      if j = len then j else
        match str.[j] with
        | '0' .. '9' | '-' -> parse_int i (j + 1)
        | _ -> j in
    let wstart = parse_spaces 0 in
    let wend = parse_lword wstart wstart in
    let box_name = String.sub str wstart (wend - wstart) in
    let nstart = parse_spaces wend in
    let nend = parse_int nstart nstart in
    let indent =
      if nstart = nend then 0 else
        try int_of_string (String.sub str nstart (nend - nstart))
        with Failure _ -> invalid_box () in
    let exp_end = parse_spaces nend in
    if exp_end <> len then invalid_box ();
    let box_type = match box_name with
      | "" | "b" -> Pp_box
      | "h"      -> Pp_hbox
      | "v"      -> Pp_vbox
      | "hv"     -> Pp_hvbox
      | "hov"    -> Pp_hovbox
      | _        -> invalid_box () in
    (indent, box_type)

