(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                          Benoit Vaugon, ENSTA                          *)
(*                                                                        *)
(*   Copyright 2014 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* No comments, OCaml stdlib internal use only. *)

open CamlinternalFormatBasics

val is_in_char_set : char_set -> char -> bool
val rev_char_set : char_set -> char_set

type mutable_char_set = bytes
val create_char_set : unit -> mutable_char_set
val add_in_char_set : mutable_char_set -> char -> unit
val freeze_char_set : mutable_char_set -> char_set

type ('b, 'c) acc_formatting_gen =
  | Acc_open_tag of ('b, 'c) acc
  | Acc_open_box of ('b, 'c) acc

and ('b, 'c) acc =
  | Acc_formatting_lit of ('b, 'c) acc * formatting_lit
  | Acc_formatting_gen of ('b, 'c) acc * ('b, 'c) acc_formatting_gen
  | Acc_string_literal of ('b, 'c) acc * string
  | Acc_char_literal   of ('b, 'c) acc * char
  | Acc_data_string    of ('b, 'c) acc * string
  | Acc_data_char      of ('b, 'c) acc * char
  | Acc_delay          of ('b, 'c) acc * ('b -> 'c)
  | Acc_flush          of ('b, 'c) acc
  | Acc_invalid_arg    of ('b, 'c) acc * string
  | End_of_acc

type ('a, 'b) heter_list =
  | Cons : 'c * ('a, 'b) heter_list -> ('c -> 'a, 'b) heter_list
  | Nil : ('b, 'b) heter_list

val make_printf :
  (('b, 'c) acc -> 'd) -> ('b, 'c) acc ->
  ('a, 'b, 'c, 'c, 'c, 'd) CamlinternalFormatBasics.fmt -> 'a

val make_iprintf : ('s -> 'f) -> 's -> ('a, 'b, 'c, 'd, 'e, 'f) fmt -> 'a

val output_acc : out_channel -> (out_channel, unit) acc -> unit
val bufput_acc : Buffer.t -> (Buffer.t, unit) acc -> unit
val strput_acc : Buffer.t -> (unit, string) acc -> unit

val char_of_iconv : CamlinternalFormatBasics.int_conv -> char
val string_of_formatting_lit : CamlinternalFormatBasics.formatting_lit -> string

val open_box_of_string : string -> int * block_type
